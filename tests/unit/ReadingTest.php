<?php

namespace ekkosense;

use DateTime;
use PHPUnit_Framework_TestCase;


class ReadingTest extends PHPUnit_Framework_TestCase {
    const EXAMPLE_DATETIME_STRING = '2014-10-11 19:59:55';
    const EXAMPLE_READING = 12.3;
    const EXAMPLE_SENSORID = 1;

    /**
     * @test
     */
    public function getDatetime_returnsConstructorParameter() {
        $this->assertEquals(new DateTime(self::EXAMPLE_DATETIME_STRING),
                            $this->reading->getDatetime());
    }

    /**
     * @test
     */
    public function getReading_returnsConstructorParameter() {
        $this->assertEquals(self::EXAMPLE_READING,
                            $this->reading->getReading());
    }

    /**
     * @test
     */
    public function getSensorid_returnsConstructorParameter() {
        $this->assertEquals(self::EXAMPLE_SENSORID,
                            $this->reading->getSensorid());
    }

    /**
     * @before
     */
    public function setUp() {
        $this->reading =
                new Reading(self::EXAMPLE_SENSORID,
                            self::EXAMPLE_READING,
                            new DateTime(self::EXAMPLE_DATETIME_STRING));
    }
    /**
     * @type Reading
     */
    private $reading;
}
