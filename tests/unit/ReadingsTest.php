<?php

namespace ekkosense;

use DateTime;
use PHPUnit_Framework_MockObject_MockObject;
use PHPUnit_Framework_TestCase;


class ReadingsTest extends PHPUnit_Framework_TestCase {
    const EXAMPLE_DATETIME_STRING       = '2014-10-11 18:38:50';
    const EXAMPLE_OTHER_DATETIME_STRING = '2014-10-10 09:30:00';
    const EXAMPLE_OTHER_READING         = 0.4;
    const EXAMPLE_OTHER_SENSORID        = 2;
    const EXAMPLE_READING               = 12.3;
    const EXAMPLE_SENSORID              = 1;

    /**
     * @test
     */
    public function addBulk_callsInsertBulk_onStorage() {
        $sensorid1 = self::EXAMPLE_SENSORID;
        $reading1  = self::EXAMPLE_READING;
        $datetime1 = self::EXAMPLE_DATETIME_STRING;

        $sensorid2 = self::EXAMPLE_OTHER_SENSORID;
        $reading2  = self::EXAMPLE_OTHER_READING;
        $datetime2 = self::EXAMPLE_OTHER_DATETIME_STRING;

        $data = [['sensorid' => $sensorid1,
                  'reading'  => $reading1,
                  'datetime' => $datetime1,],
                 ['sensorid' => $sensorid2,
                  'reading'  => $reading2,
                  'datetime' => $datetime2,],];

        $this->storage->expects($this->once())
                      ->method('insertBulk')
                      ->with('reading', $data);

        $this->readings->addBulk([new Reading($sensorid1,
                                              $reading1,
                                              new DateTime($datetime1)),
                                  new Reading($sensorid2,
                                              $reading2,
                                              new DateTime($datetime2)),]);
    }

    /**
     * @test
     */
    public function addValues_insertsInDatabase() {
        $data = ['sensorid' => self::EXAMPLE_SENSORID,
                 'reading'  => self::EXAMPLE_READING,
                 'datetime' => self::EXAMPLE_DATETIME_STRING,];

        $this->storage->expects($this->once())
                      ->method('insert')
                      ->with('reading', $data);

        $this->readings->addValues(self::EXAMPLE_SENSORID,
                                   self::EXAMPLE_READING,
                                   new DateTime(self::EXAMPLE_DATETIME_STRING));
    }

    /**
     * @test
     */
    public function add_callsInsertBulk_onStorage() {
        $data = [['sensorid' => self::EXAMPLE_SENSORID,
                  'reading'  => self::EXAMPLE_READING,
                  'datetime' => self::EXAMPLE_DATETIME_STRING,],];

        $this->storage->expects($this->once())
                      ->method('insertBulk')
                      ->with('reading', $data);

        $this->readings->add($this->exampleReading());
    }

    /**
     * @test
     */
    public function getLatestForSensor_defersToGetGroupedByHighest_onStorage() {
        $storageReturn = [['sensorid' => self::EXAMPLE_SENSORID,
                           'reading'  => self::EXAMPLE_READING,
                           'datetime' => self::EXAMPLE_DATETIME_STRING,],];

        $conditions = ['sensorid' => [self::EXAMPLE_SENSORID],
                       'datetime' => new ConditionalInRange(),];

        $this->storage->expects($this->once())
                      ->method('getGroupedByHighest')
                      ->with('reading', 'sensorid', 'datetime', $conditions)
                      ->will($this->returnValue($storageReturn));

        $expected = $this->exampleReading();
        $actual   = $this->readings->getLatestForSensor(self::EXAMPLE_SENSORID);

        $this->assertEquals($expected, $actual);
    }

    /**
     * @return Reading
     */
    protected function exampleReading() {
        return new Reading(self::EXAMPLE_SENSORID,
                           self::EXAMPLE_READING,
                           new DateTime(self::EXAMPLE_DATETIME_STRING));
    }

    /**
     * @test
     */
    public function getLatestForSensorsInDateRange_defersToGetGroupedByHighest_onStorage() {
        $storageReturn = [['sensorid' => self::EXAMPLE_SENSORID,
                           'reading'  => self::EXAMPLE_READING,
                           'datetime' => self::EXAMPLE_DATETIME_STRING,],
                          ['sensorid' => self::EXAMPLE_OTHER_SENSORID,
                           'reading'  => self::EXAMPLE_OTHER_READING,
                           'datetime' => self::EXAMPLE_OTHER_DATETIME_STRING,],];

        $from = '2000-01-02 03:04:05';
        $to   = '9876-05-04 03:02:01';

        $conditions =
                ['sensorid' => [self::EXAMPLE_SENSORID,
                                self::EXAMPLE_OTHER_SENSORID],
                 'datetime' => new ConditionalInRange($from, $to),];

        $this->storage->expects($this->once())
                      ->method('getGroupedByHighest')
                      ->with('reading', 'sensorid', 'datetime', $conditions)
                      ->will($this->returnValue($storageReturn));

        $expected =
                [self::EXAMPLE_SENSORID       => new Reading(self::EXAMPLE_SENSORID,
                                                             self::EXAMPLE_READING,
                                                             new DateTime(self::EXAMPLE_DATETIME_STRING)),
                 self::EXAMPLE_OTHER_SENSORID => new Reading(self::EXAMPLE_OTHER_SENSORID,
                                                             self::EXAMPLE_OTHER_READING,
                                                             new DateTime(self::EXAMPLE_OTHER_DATETIME_STRING)),];

        $actual =
                $this->readings->getLatestForSensorsInDateRange([self::EXAMPLE_SENSORID,
                                                                 self::EXAMPLE_OTHER_SENSORID],
                                                                new DateTime($to),
                                                                new DateTime($from));

        $this->assertEquals($expected, $actual);
    }

    /**
     * @test
     */
    public function getLatestForSensors_defersToGetGroupedByHighest_onStorage() {
        $storageReturn = [['sensorid' => self::EXAMPLE_SENSORID,
                           'reading'  => self::EXAMPLE_READING,
                           'datetime' => self::EXAMPLE_DATETIME_STRING,],
                          ['sensorid' => self::EXAMPLE_OTHER_SENSORID,
                           'reading'  => self::EXAMPLE_OTHER_READING,
                           'datetime' => self::EXAMPLE_OTHER_DATETIME_STRING,],];

        $conditions =
                ['sensorid' => [self::EXAMPLE_SENSORID,
                                self::EXAMPLE_OTHER_SENSORID],
                 'datetime' => new ConditionalInRange(),];

        $this->storage->expects($this->once())
                      ->method('getGroupedByHighest')
                      ->with('reading', 'sensorid', 'datetime', $conditions)
                      ->will($this->returnValue($storageReturn));

        $expected =
                [self::EXAMPLE_SENSORID       => new Reading(self::EXAMPLE_SENSORID,
                                                             self::EXAMPLE_READING,
                                                             new DateTime(self::EXAMPLE_DATETIME_STRING)),
                 self::EXAMPLE_OTHER_SENSORID => new Reading(self::EXAMPLE_OTHER_SENSORID,
                                                             self::EXAMPLE_OTHER_READING,
                                                             new DateTime(self::EXAMPLE_OTHER_DATETIME_STRING)),];
        $actual   =
                $this->readings->getLatestForSensors([self::EXAMPLE_SENSORID,
                                                      self::EXAMPLE_OTHER_SENSORID]);

        $this->assertEquals($expected, $actual);
    }

    /**
     * @test
     */
    public function getLatestValueForSensorInDateRange_defersToGetGroupedByHighest_onStorage() {
        $storageReturn = [['sensorid' => self::EXAMPLE_SENSORID,
                           'reading'  => self::EXAMPLE_READING,
                           'datetime' => self::EXAMPLE_DATETIME_STRING,],];

        $from = '2000-01-02 03:04:05';
        $to   = '9876-05-04 03:02:01';

        $conditions = ['sensorid' => [self::EXAMPLE_SENSORID],
                       'datetime' => new ConditionalInRange($from, $to),];

        $this->storage->expects($this->once())
                      ->method('getGroupedByHighest')
                      ->with('reading', 'sensorid', 'datetime', $conditions)
                      ->will($this->returnValue($storageReturn));

        $reading =
                $this->readings->getLatestValueForSensorInDateRange(self::EXAMPLE_SENSORID,
                                                                    new DateTime($to),
                                                                    new DateTime($from));

        $this->assertEquals(self::EXAMPLE_READING, $reading);
    }

    /**
     * @test
     */
    public function getLatestValueForSensor_defersToGetGroupedByHighest_onStorage() {
        $storageReturn = [['sensorid' => self::EXAMPLE_SENSORID,
                           'reading'  => self::EXAMPLE_READING,
                           'datetime' => self::EXAMPLE_DATETIME_STRING,],];

        $conditions = ['sensorid' => [self::EXAMPLE_SENSORID],
                       'datetime' => new ConditionalInRange(),];

        $this->storage->expects($this->once())
                      ->method('getGroupedByHighest')
                      ->with('reading', 'sensorid', 'datetime', $conditions)
                      ->will($this->returnValue($storageReturn));

        $reading =
                $this->readings->getLatestValueForSensor(self::EXAMPLE_SENSORID);
        $this->assertEquals(self::EXAMPLE_READING, $reading);
    }

    /**
     * @test
     */
    public function getLatestValuesForSensors_defersToGetGroupedByHighest_onStorage() {
        $storageReturn = [['sensorid' => self::EXAMPLE_SENSORID,
                           'reading'  => self::EXAMPLE_READING,
                           'datetime' => self::EXAMPLE_DATETIME_STRING,],
                          ['sensorid' => self::EXAMPLE_OTHER_SENSORID,
                           'reading'  => self::EXAMPLE_OTHER_READING,
                           'datetime' => self::EXAMPLE_OTHER_DATETIME_STRING,],];

        $conditions =
                ['sensorid' => [self::EXAMPLE_SENSORID,
                                self::EXAMPLE_OTHER_SENSORID],
                 'datetime' => new ConditionalInRange(),];

        $this->storage->expects($this->once())
                      ->method('getGroupedByHighest')
                      ->with('reading', 'sensorid', 'datetime', $conditions)
                      ->will($this->returnValue($storageReturn));

        $expected = [self::EXAMPLE_SENSORID       => self::EXAMPLE_READING,
                     self::EXAMPLE_OTHER_SENSORID => self::EXAMPLE_OTHER_READING,];
        $actual   =
                $this->readings->getLatestValuesForSensors([self::EXAMPLE_SENSORID,
                                                            self::EXAMPLE_OTHER_SENSORID]);

        $this->assertEquals($expected, $actual);
    }

    /**
     * @test
     */
    public function getUnresponsiveSensors_defersTo___() {
        $now   = '2014-03-04 23:43:11';
        $limit = '2014-03-04 20:43:11';

        $data = [['sensorid' => self::EXAMPLE_SENSORID,
                  'reading'  => self::EXAMPLE_READING,
                  'datetime' => self::EXAMPLE_DATETIME_STRING,],
                 ['sensorid' => self::EXAMPLE_OTHER_SENSORID,
                  'reading'  => self::EXAMPLE_OTHER_READING,
                  'datetime' => self::EXAMPLE_OTHER_DATETIME_STRING,],];

        $conditions = ['datetime' => new ConditionalInRange($limit, $now),];

        $this->storage->expects($this->once())
                      ->method('notMatching')
                      ->with('reading', 'sensorid', $conditions)
                      ->will($this->returnValue($data));

        $expected = [self::EXAMPLE_SENSORID, self::EXAMPLE_OTHER_SENSORID];
        $actual   = $this->readings->getUnresponsiveSensors(new DateTime($now));

        $this->assertEquals($expected, $actual);
    }

    /**
     * @before
     */
    public function setup() {
        $this->storage  = $this->getMock('ekkosense\Storage');
        $this->readings = new Readings($this->storage);
    }
    /**
     * @type Readings
     */
    private $readings;

    /**
     * @type PHPUnit_Framework_MockObject_MockObject|Storage
     */
    private $storage;
}
