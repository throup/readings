<?php
spl_autoload_register(function ($classname) {
    $parts     = explode('\\', $classname);
    $namespace = array_shift($parts);
    if ($namespace == 'ekkosense') {
        $filename = dirname(__DIR__) . '/src/' . implode('/', $parts) . '.php';
        if (file_exists($filename)) {
            require_once($filename);
        }
    }
});


class IntegrationTestConfig {
    /**
     * @return string
     */
    public static function getHost() {
        return self::$host;
    }

    /**
     * @param string $host
     */
    public static function setHost($host) {
        self::$host = $host;
    }

    /**
     * @return string
     */
    public static function getPassword() {
        return self::$password;
    }

    /**
     * @param string $password
     */
    public static function setPassword($password) {
        self::$password = $password;
    }

    /**
     * @return string
     */
    public static function getSchema() {
        return self::$schema;
    }

    /**
     * @param string $schema
     */
    public static function setSchema($schema) {
        self::$schema = $schema;
    }

    /**
     * @return string
     */
    public static function getUsername() {
        return self::$username;
    }

    /**
     * @param string $username
     */
    public static function setUsername($username) {
        self::$username = $username;
    }
    private static $host     = 'localhost';
    private static $password = 'es';
    private static $schema   = 'es';
    private static $username = 'es';
}


$localconfig = dirname(__DIR__) . '/config/integration.php';
if (file_exists($localconfig)) {
    require_once($localconfig);
}
