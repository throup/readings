<?php

namespace ekkosense;

use InvalidArgumentException;
use PHPUnit_Extensions_Database_DataSet_AbstractDataSet;
use PHPUnit_Extensions_Database_DataSet_DefaultTable;
use PHPUnit_Extensions_Database_DataSet_DefaultTableIterator;
use PHPUnit_Extensions_Database_DataSet_DefaultTableMetaData;

/**
 * Utility class taken from https://phpunit.de/manual/current/en/database.html
 *
 * @access private
 */
class MyApp_DbUnit_ArrayDataSet
        extends PHPUnit_Extensions_Database_DataSet_AbstractDataSet {
    /**
     * @param array $data
     */
    public function __construct(array $data) {
        foreach ($data AS $tableName => $rows) {
            $columns = [];
            if (isset($rows[0])) {
                $columns = array_keys($rows[0]);
            }

            $metaData =
                    new PHPUnit_Extensions_Database_DataSet_DefaultTableMetaData($tableName,
                                                                                 $columns);
            $table    =
                    new PHPUnit_Extensions_Database_DataSet_DefaultTable($metaData);

            foreach ($rows AS $row) {
                $table->addRow($row);
            }
            $this->tables[$tableName] = $table;
        }
    }

    public function getTable($tableName) {
        if (!isset($this->tables[$tableName])) {
            throw new InvalidArgumentException("$tableName is not a table in the current database.");
        }

        return $this->tables[$tableName];
    }

    protected function createIterator($reverse = false) {
        return new PHPUnit_Extensions_Database_DataSet_DefaultTableIterator($this->tables,
                                                                            $reverse);
    }
    /**
     * @var array
     */
    protected $tables = [];
}
