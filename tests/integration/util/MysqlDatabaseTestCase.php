<?php

namespace ekkosense;

require_once('MyApp_DbUnit_ArrayDataSet.php');

use IntegrationTestConfig;
use PDO;
use PHPUnit_Extensions_Database_DataSet_IDataSet;
use PHPUnit_Extensions_Database_DB_IDatabaseConnection;
use PHPUnit_Extensions_Database_TestCase;

abstract class MysqlDatabaseTestCase
        extends PHPUnit_Extensions_Database_TestCase {
    const TABLENAME = 'reading';

    /**
     * @param $data
     */
    protected function assertDataInTestTable($data) {
        $queryTable =
                $this->getConnection()
                     ->createQueryTable(self::TABLENAME,
                                        'SELECT * FROM ' . self::TABLENAME);
        $expectedTable =
                (new MyApp_DbUnit_ArrayDataSet([self::TABLENAME => $data,]))->getTable(self::TABLENAME);

        $this->assertTablesEqual($expectedTable, $queryTable);
    }

    /**
     * Returns the test database connection.
     *
     * @return PHPUnit_Extensions_Database_DB_IDatabaseConnection
     */
    protected function getConnection() {
        $pdo =
                new PDO('mysql:host=' .
                        IntegrationTestConfig::getHost() .
                        ';dbname=' .
                        IntegrationTestConfig::getSchema(),
                        IntegrationTestConfig::getUsername(),
                        IntegrationTestConfig::getPassword());
        return $this->createDefaultDBConnection($pdo,
                                                IntegrationTestConfig::getSchema());
    }

    /**
     * Returns the test dataset.
     *
     * @return PHPUnit_Extensions_Database_DataSet_IDataSet
     */
    protected function getDataSet() {
        return new MyApp_DbUnit_ArrayDataSet(['reading' => []]);
    }
}
