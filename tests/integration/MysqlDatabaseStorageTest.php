<?php

namespace ekkosense;

use IntegrationTestConfig;

require_once('util/MysqlDatabaseTestCase.php');


class MysqlDatabaseStorageTest extends MysqlDatabaseTestCase {

    /**
     * @test
     */
    public function MysqlDatabaseStorage_implements_Storage() {
        $f = function (Storage $storage) {
            $storage;
        };

        $f($this->storage);
    }

    /**
     * @test
     */
    public function insertBulk_doesInsertData() {
        $records = [['sensorid' => 6,
                     'datetime' => '2014-10-11 21:30:00',
                     'reading'  => 0.04,],
                    ['sensorid' => 7,
                     'datetime' => '2012-06-25 09:30:00',
                     'reading'  => 2.34,],];

        $this->storage->insertBulk(self::TABLENAME, $records);
        $this->assertDataInTestTable($records);
    }

    /**
     * @test
     */
    public function insert_doesInsertData() {
        $record = ['sensorid' => 3,
                   'datetime' => '2014-10-11 21:30:00',
                   'reading'  => 3.52,];

        $this->storage->insert(self::TABLENAME, $record);
        $this->assertDataInTestTable([$record]);
    }

    /**
     * @before
     */
    public function setUp() {
        parent::setUp();
        $this->storage =
                new MysqlDatabaseStorage(IntegrationTestConfig::getHost(),
                                         IntegrationTestConfig::getUsername(),
                                         IntegrationTestConfig::getPassword(),
                                         IntegrationTestConfig::getSchema());
    }
    /**
     * @type MysqlDatabaseStorage
     */
    private $storage;
}
