<?php

namespace ekkosense;

use DateTime;
use IntegrationTestConfig;

require_once('util/MysqlDatabaseTestCase.php');


class Readings_MysqlDatabaseStorageTest extends MysqlDatabaseTestCase {
    /**
     * @test
     */
    public function addBulk_insertsIntoDatabase() {
        $sensorid1 = 4;
        $datetime1 = '2012-10-13 14:25:30';
        $reading1  = 11.2;

        $sensorid2 = 6;
        $datetime2 = '2014-10-13 14:25:30';
        $reading2  = 21.2;

        $this->readings->addBulk([new Reading($sensorid1,
                                              $reading1,
                                              new DateTime($datetime1)),
                                  new Reading($sensorid2,
                                              $reading2,
                                              new DateTime($datetime2)),]);

        $this->assertDataInTestTable([['sensorid' => $sensorid1,
                                       'datetime' => $datetime1,
                                       'reading'  => $reading1,],
                                      ['sensorid' => $sensorid2,
                                       'datetime' => $datetime2,
                                       'reading'  => $reading2,],]);
    }

    /**
     * @test
     */
    public function addValues_insertsIntoDatabase() {
        $sensorid = 1;
        $datetime = '2013-11-23 04:24:32';
        $reading  = 23.4;

        $this->readings->addValues($sensorid,
                                   $reading,
                                   new DateTime($datetime));

        $this->assertDataInTestTable([['sensorid' => $sensorid,
                                       'datetime' => $datetime,
                                       'reading'  => $reading,]]);
    }

    /**
     * @test
     */
    public function add_insertsIntoDatabase() {
        $sensorid = 1;
        $datetime = '2013-11-23 04:24:32';
        $reading  = 23.4;

        $this->readings->add(new Reading($sensorid,
                                         $reading,
                                         new DateTime($datetime)));

        $this->assertDataInTestTable([['sensorid' => $sensorid,
                                       'datetime' => $datetime,
                                       'reading'  => $reading,]]);
    }

    /**
     * @test
     */
    public function getLatestForSensor_returnsLatest() {
        $this->readings->addBulk($this->sampleData());

        $expected = new Reading(12, 23.0, new DateTime('2014-02-24 11:00:00'));
        $actual   = $this->readings->getLatestForSensor(12);

        $this->assertEquals($expected, $actual);
    }

    /**
     * @test
     */
    public function getLatestForSensorsInDateRange_returnsLatest() {
        $this->readings->addBulk($this->sampleData());

        $sensorids = [12, 14];
        $from      = new DateTime('2014-01-01 00:00:00');
        $to        = new DateTime('2014-02-01 00:00:00');

        $expected =
                [12 => new Reading(12,
                                   19.0,
                                   new DateTime('2014-01-24 10:00:00')),
                 14 => new Reading(14,
                                   0.09,
                                   new DateTime('2014-01-24 10:00:00')),];
        $actual   =
                $this->readings->getLatestForSensorsInDateRange($sensorids,
                                                                $to,
                                                                $from);

        $this->assertEquals($expected, $actual);
    }

    /**
     * @test
     */
    public function getLatestForSensors_returnsLatest() {
        $this->readings->addBulk($this->sampleData());

        $expected =
                [12 => new Reading(12,
                                   23.0,
                                   new DateTime('2014-02-24 11:00:00')),
                 14 => new Reading(14,
                                   0.074,
                                   new DateTime('2014-02-24 11:00:00')),];
        $actual   = $this->readings->getLatestForSensors([12, 14]);

        $this->assertEquals($expected, $actual);
    }

    /**
     * @test
     */
    public function getLatestValueForSensorInDateRange_returnsLatest() {
        $this->readings->addBulk($this->sampleData());

        $sensorid = 12;
        $from     = new DateTime('2014-01-01 00:00:00');
        $to       = new DateTime('2014-02-01 00:00:00');

        $value =
                $this->readings->getLatestValueForSensorInDateRange($sensorid,
                                                                    $to,
                                                                    $from);

        $this->assertEquals(19.0, $value);
    }

    /**
     * @test
     */
    public function getLatestValueForSensor_returnsLatest() {
        $this->readings->addBulk($this->sampleData());

        $value = $this->readings->getLatestValueForSensor(12);
        $this->assertEquals(23.0, $value);
    }

    /**
     * @return array
     */
    protected function sampleData() {
        return [new Reading(12, 23.0, new DateTime('2014-02-24 11:00:00')),
                new Reading(12, 22.0, new DateTime('2014-02-24 10:00:00')),
                new Reading(12, 19.0, new DateTime('2014-01-24 10:00:00')),
                new Reading(12, 17.0, new DateTime('2014-01-14 10:00:00')),
                new Reading(13, 12.4, new DateTime('2014-02-24 11:00:00')),
                new Reading(13, 13.4, new DateTime('2014-01-24 10:00:00')),
                new Reading(13, 14.4, new DateTime('2014-01-14 10:00:00')),
                new Reading(14, 0.074, new DateTime('2014-02-24 11:00:00')),
                new Reading(14, 0.08, new DateTime('2014-02-24 10:00:00')),
                new Reading(14, 0.09, new DateTime('2014-01-24 10:00:00')),
                new Reading(14, 0.06, new DateTime('2014-01-14 10:00:00')),
                new Reading(15, 4.2, new DateTime('2014-01-24 10:00:00')),
                new Reading(16, 13.5, new DateTime('2014-02-24 09:30:00')),
                new Reading(17, 2.4, new DateTime('2014-02-24 08:30:00')),];
    }

    /**
     * @test
     */
    public function getLatestValuesForSensors_returnsLatest() {
        $this->readings->addBulk($this->sampleData());

        $expected = [12 => 23.0,
                     14 => 0.074,];
        $actual   = $this->readings->getLatestValuesForSensors([12, 14]);

        $this->assertEquals($expected, $actual);
    }

    /**
     * @test
     */
    public function getUnresponsiveSensors_returns() {
        $this->readings->addBulk($this->sampleData());
        $now = new DateTime('2014-02-24 12:00:00');

        $expected = [15, 17];
        $actual   = $this->readings->getUnresponsiveSensors($now);

        $this->assertEquals($expected, $actual);
    }

    /**
     * @before
     */
    public function setUp() {
        parent::setUp();
        $this->storage =
                new MysqlDatabaseStorage(IntegrationTestConfig::getHost(),
                                         IntegrationTestConfig::getUsername(),
                                         IntegrationTestConfig::getPassword(),
                                         IntegrationTestConfig::getSchema());
        $this->readings = new Readings($this->storage);
    }
    /**
     * @type Readings
     */
    private $readings;

    /**
     * @type MysqlDatabaseStorage
     */
    private $storage;
}
