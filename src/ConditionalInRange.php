<?php

namespace ekkosense;

class ConditionalInRange {
    public function __construct($from = '0001-01-01 00:00:00',
                                $to = '9999-12-31 23:59:59') {
        $this->from = $from;
        $this->to   = $to;
    }

    public function getFrom() {
        return $this->from;
    }

    public function getTo() {
        return $this->to;
    }

    private $from;
    private $to;
}
