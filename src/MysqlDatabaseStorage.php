<?php

namespace ekkosense;

use PDO;

class MysqlDatabaseStorage implements Storage {
    public function __construct($host, $user, $password, $schema) {
        $this->pdo =
                new PDO("mysql:host={$host};dbname={$schema}",
                        $user,
                        $password);
        $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }

    /**
     * @param  string $tablename
     * @param  string $groupingfield
     * @param  string $orderfield
     * @param  array  $conditions
     * @return array
     */
    public function getGroupedByHighest($tablename,
                                        $groupingfield,
                                        $orderfield,
                                        array $conditions = []) {
        $wheresql = $this->generateWhereSql($conditions);
        $data     = $this->generateWhereParameters($conditions);
        $sql      = "SELECT *
                       FROM (
                             SELECT *
                               FROM {$tablename}
                              WHERE {$wheresql}
                           ORDER BY {$orderfield} DESC
                            ) sub
                   GROUP BY sub.{$groupingfield}";

        $statement = $this->pdo->prepare($sql);
        $statement->execute($data);
        return $statement->fetchAll();
    }

    /**
     * @param  array $conditions
     * @return string
     * @todo Refactor all of the conditionals into Conditional classes. The
     *       "instanceof" check here is a very non-OOP code smell.
     */
    protected function generateWhereSql(array $conditions) {
        $wheresql = '1';
        foreach ($conditions as $field => $value) {
            if (is_array($value)) {
                $wheresql .=
                        " AND {$field} IN (" .
                        implode(',', array_fill(0, count($value), '?')) .
                        ")";
            } else if ($value instanceof ConditionalInRange) {
                $wheresql .= " AND {$field} >= ?" . " AND {$field} < ?";
            } else {
                $wheresql .= " AND {$field} = ?";
            }
        }
        return $wheresql;
    }

    /**
     * @param  array $conditions
     * @return array
     * @todo Refactor all of the conditionals into Conditional classes. The
     *       "instanceof" check here is a very non-OOP code smell.
     */
    protected function generateWhereParameters(array $conditions) {
        $params = [];
        foreach ($conditions as $value) {
            if (is_array($value)) {
                $params = array_merge($params, array_values($value));
            } else if ($value instanceof ConditionalInRange) {
                $params[] = $value->getFrom();
                $params[] = $value->getTo();
            } else {
                $params[] = $value;
            }
        }
        return $params;
    }

    /**
     * @param string $tablename
     * @param array  $data
     */
    public function insert($tablename, array $data) {
        $this->insertBulk($tablename, [$data]);
    }

    /**
     * @param string $tablename
     * @param array  $data
     */
    public function insertBulk($tablename, array $data) {
        $columns      = implode(',', array_keys(reset($data)));
        $placeholder  = implode(',', array_fill(0, count(reset($data)), '?'));
        $placeholders =
                implode(',', array_fill(0, count($data), "({$placeholder})"));

        $sql = "INSERT INTO {$tablename}
                            ({$columns})
                     VALUES {$placeholders}";

        $values = [];
        foreach ($data as $datum) {
            $values = array_merge($values, array_values($datum));
        }

        $statement = $this->pdo->prepare($sql);
        $statement->execute($values);
    }

    /**
     * @param string $tablename
     * @param string $groupingfield
     * @param array  $conditions
     * @return array
     */
    public function notMatching($tablename,
                                $groupingfield,
                                array $conditions = []) {
        $wheresql = $this->generateWhereSql($conditions);
        $data     = $this->generateWhereParameters($conditions);

        $sql = "SELECT complete.*
                  FROM (
                        SELECT *
                          FROM {$tablename}
                      GROUP BY {$groupingfield}
                       ) complete
             LEFT JOIN (
                        SELECT *
                          FROM {$tablename}
                         WHERE {$wheresql}
                      GROUP BY {$groupingfield}
                       ) matching
                    ON complete.{$groupingfield} = matching.{$groupingfield}
                 WHERE matching.{$groupingfield} IS NULL";

        $statement = $this->pdo->prepare($sql);
        $statement->execute($data);
        return $statement->fetchAll();
    }
    /**
     * @type PDO
     */
    private $pdo;
}
