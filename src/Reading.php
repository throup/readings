<?php

namespace ekkosense;

use DateTime;

class Reading {
    public function __construct($sensorid, $reading, DateTime $datetime) {
        $this->sensorid = (int) $sensorid;
        $this->reading  = (float) $reading;
        $this->datetime = $datetime;
    }

    /**
     * @return DateTime
     */
    public function getDatetime() {
        return $this->datetime;
    }

    /**
     * @return float
     */
    public function getReading() {
        return (float) $this->reading;
    }

    /**
     * @return int
     */
    public function getSensorid() {
        return (int) $this->sensorid;
    }
    /**
     * @type DateTime
     */
    private $datetime;
    /**
     * @type float
     */
    private $reading;
    /**
     * @type int
     */
    private $sensorid;
}
