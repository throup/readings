<?php

namespace ekkosense;

use DateTime;

class Readings {
    const DATETIME_DISTANT_PAST = '0001-01-01 00:00:00';
    const DATETIME_FAR_FUTURE   = '9999-12-31 23:59:59';
    const DATETIME_MYSQL_FORMAT = 'Y-m-d H:i:s';
    const TABLENAME             = 'reading';

    /**
     * @param Storage $storage
     */
    public function __construct(Storage $storage) {
        $this->storage = $storage;
    }

    /**
     * Add the given reading to the data store.
     *
     * @param Reading $reading
     */
    public function add(Reading $reading) {
        $this->addBulk([$reading]);
    }

    /**
     * Add the given array of Readings to the data store.
     *
     * @param array|Reading[] $readings
     */
    public function addBulk(array $readings) {
        $data = [];
        foreach ($readings as $reading) {
            $data[] = ['sensorid' => $reading->getSensorid(),
                       'reading'  => $reading->getReading(),
                       'datetime' => $reading->getDatetime()
                                             ->format(self::DATETIME_MYSQL_FORMAT),];
        }
        $this->storage->insertBulk(self::TABLENAME, $data);
    }

    /**
     * Add the given values, as a reading, to the data store.
     *
     * @param int      $sensorid
     * @param string   $reading
     * @param DateTime $datetime
     */
    public function addValues($sensorid, $reading, DateTime $datetime) {
        $data = ['sensorid' => $sensorid,
                 'reading'  => $reading,
                 'datetime' => $datetime->format(self::DATETIME_MYSQL_FORMAT),];
        $this->storage->insert(self::TABLENAME, $data);
    }

    /**
     * Get the latest Readings for the sensors specified by the given array of
     * ids.
     *
     * @param  array|int[] $sensorids
     * @return array|Reading[]
     */
    public function getLatestForSensors(array $sensorids) {
        $to   = new DateTime(self::DATETIME_FAR_FUTURE);
        $from = new DateTime(self::DATETIME_DISTANT_PAST);

        return $this->getLatestForSensorsInDateRange($sensorids, $to, $from);
    }

    /**
     * Get the latest Readings for the sensors specified by the given array of
     * ids. Limit to readings recorded between the given DateTimes, $from
     * (inclusive) and $to (exclusive).
     *
     * @param  array|int[] $sensorids
     * @param  DateTime    $to
     * @param  DateTime    $from
     * @return array|Reading[]
     */
    public function getLatestForSensorsInDateRange(array $sensorids,
                                                   DateTime $to,
                                                   DateTime $from) {
        $conditions = ['sensorid' => $sensorids,
                       'datetime' => new ConditionalInRange($from->format(self::DATETIME_MYSQL_FORMAT),
                                                            $to->format(self::DATETIME_MYSQL_FORMAT)),];

        $records  =
                $this->storage->getGroupedByHighest(self::TABLENAME,
                                                    'sensorid',
                                                    'datetime',
                                                    $conditions);
        $readings = [];
        foreach ($records as $data) {
            $reading =
                    new Reading($data['sensorid'],
                                $data['reading'],
                                new DateTime($data['datetime']));

            $readings[$reading->getSensorid()] = $reading;
        }

        return $readings;
    }

    /**
     * Get the latest value for the given sensor.
     *
     * @param  int $sensorid
     * @return float
     */
    public function getLatestValueForSensor($sensorid) {
        $reading = $this->getLatestForSensor($sensorid);
        return (float) $reading->getReading();
    }

    /**
     * Get the latest Reading for the given sensor.
     *
     * @param  int $sensorid
     * @return Reading
     */
    public function getLatestForSensor($sensorid) {
        $to   = new DateTime(self::DATETIME_FAR_FUTURE);
        $from = new DateTime(self::DATETIME_DISTANT_PAST);

        return $this->getLatestForSensorInDateRange($sensorid, $to, $from);
    }

    /**
     * Get the latest Reading for the given sensor.
     * Limit to readings recorded between the given DateTimes, $from
     * (inclusive) and $to (exclusive).
     *
     * @param  int $sensorid
     * @return Reading
     */
    public function getLatestForSensorInDateRange($sensorid,
                                                  DateTime $to,
                                                  DateTime $from) {
        $readings =
                $this->getLatestForSensorsInDateRange([$sensorid], $to, $from);
        return $readings[$sensorid];
    }

    /**
     * Get the latest value for the given sensor.
     * Limit to readings recorded between the given DateTimes, $from
     * (inclusive) and $to (exclusive).
     *
     * @param  int      $sensorid
     * @param  DateTime $to
     * @param  DateTime $from
     * @return float
     */
    public function getLatestValueForSensorInDateRange($sensorid,
                                                       DateTime $to,
                                                       DateTime $from) {
        $readings =
                $this->getLatestForSensorsInDateRange([$sensorid], $to, $from);
        return (float) $readings[$sensorid]->getReading();
    }

    /**
     * Get the latest values for the sensors specified by the given array of
     * ids.
     *
     * @param  array|int $sensorids
     * @return array|float[]
     */
    public function getLatestValuesForSensors(array $sensorids) {
        $to   = new DateTime(self::DATETIME_FAR_FUTURE);
        $from = new DateTime(self::DATETIME_DISTANT_PAST);

        return $this->getLatestValuesForSensorsInDateRange($sensorids,
                                                           $to,
                                                           $from);
    }

    /**
     * Get the latest values for the sensors specified by the given array of
     * ids. Limit to readings recorded between the given DateTimes, $from
     * (inclusive) and $to (exclusive).
     *
     * @param  array|int $sensorids
     * @return array|float[]
     */
    public function getLatestValuesForSensorsInDateRange(array $sensorids,
                                                         DateTime $to,
                                                         DateTime $from) {
        $readings =
                $this->getLatestForSensorsInDateRange($sensorids, $to, $from);
        $values   = [];
        foreach ($readings as $sensorid => $reading) {
            $values[$sensorid] = (float) $reading->getReading();
        }
        return $values;
    }

    /**
     * Identify unresponsive sensors, returning an array of sensor ids.
     * A sensor is considered unresponsive if no readings have been recorded
     * during the preceding three hours. The optional $now parameter takes a
     * DateTime representing the moment in time for which the responsive state
     * of the sensors should be assessed.
     *
     * @param  DateTime $now
     * @return array|int[]
     */
    public function getUnresponsiveSensors(DateTime $now = null) {
        if ($now === null) {
            $now = new DateTime();
        }

        $limit = clone($now);
        $limit->sub(new \DateInterval('PT3H'));

        $conditions =
                ['datetime' => new ConditionalInRange($limit->format(self::DATETIME_MYSQL_FORMAT),
                                                      $now->format(self::DATETIME_MYSQL_FORMAT))];

        $records =
                $this->storage->notMatching(self::TABLENAME,
                                            'sensorid',
                                            $conditions);

        $sensorids = [];
        foreach ($records as $record) {
            $sensorids[] = $record['sensorid'];
        }

        return $sensorids;
    }

    /**
     * @type Storage
     */
    private $storage;
}
