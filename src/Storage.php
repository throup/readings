<?php

namespace ekkosense;

interface Storage {
    /**
     * @param  string $tablename
     * @param  string $groupingfield
     * @param  string $orderfield
     * @param  array  $conditions
     * @return array
     */
    public function getGroupedByHighest($tablename,
                                        $groupingfield,
                                        $orderfield,
                                        array $conditions = []);

    /**
     * @param string $tablename
     * @param array  $data
     */
    public function insert($tablename, array $data);

    /**
     * @param string $tablename
     * @param array  $data
     */
    public function insertBulk($tablename, array $data);

    /**
     * @param string $tablename
     * @param string $groupingfield
     * @param array  $conditions
     * @return array
     */
    public function notMatching($tablename,
                                $groupingfield,
                                array $conditions = []);
}
