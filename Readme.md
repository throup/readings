# Readings class

## Brief

Readings from sensors are stored on a MySQL database table called `reading`. Here's an example of some data...

| sensorid | datetime            | reading |
|---------:|:-------------------:|---------|
|       12 | 2014-02-24 11:00:00 |  23.0   |
|       12 | 2014-02-24 10:00:00 |  22.0   |
|       13 | 2014-02-24 11:00:00 |  12.4   |
|       14 | 2014-02-24 11:00:00 |   0.074 |
|       14 | 2014-02-24 10:00:00 |   0.08  |

Design a method in PHP to manage and deliver readings. Include the following procedures:

* Add new readings, individually or in bulk, to the database
* Deliver the latest readings for an individual sensor or a group of sensors. (Pass sensor id(s) as a parameter)
* As above but over a specified time range with to and from date/time parameters.
* Return an array of unresponsive sensors by id. (An unresponsive sensor can be viewed as a sensor which does not have a reading
from the last 3 hours)


## How to use
The core functionality is implemented by the `ekkosense\Readings` class. To use the class within an application,
take the following steps:

_The contents of `src/` must be copied to the correct
path for autoloading the `ekkosense` namespace._

```
#!php
<?php

// Instantiate a `MysqlDatabaseStorage` object, passing MySQL connection settings to the constructor.
$storage = new ekkosense\MysqlDatabaseStorage($host, $user, $password, $schema);

// Instantiate a `Readings` object.
$readings = new ekkosense\Readings($storage);



// Add new readings, individually...
$reading = new ekkosense\Reading($sensorid, $value, new DateTime());
$readings->add($reading);


// (or raw values)
$readings->addValues($sensorid, $value, new DateTime());


// ... or in bulk, to the database.
$array = [
    new ekkosense\Reading($sensorid, $value, new DateTime()),
    new ekkosense\Reading($sensorid, $value, new DateTime()),
];
$readings->addBulk($array);



// Deliver the latest readings for an individual sensor...

/**
 * @type ekkosense\Reading         $reading
 */
$reading   = $readings->getLatestForSensor($sensorid)

/**
 * @type float                     $value
 */
$value     = $readings->getLatestValueForSensor($sensorid)


// ...or a group of sensors.

$sensorids = [$sensorid, $othersensorid];

/**
 * @type array|ekkosense\Reading[] $readings
 */
$readings  = $readings->getLatestForSensors($sensorids)

/**
 * @type array|float[]             $value
 */
$values    = $readings->getLatestValuesForSensors($sensorids)



// As above but over a specified time range with to and from date/time parameters.
$to   = new DateTime();
$from = new DateTime();

$reading   = $readings->getLatestForSensorInDateRange       ($sensorid,  $to, $from)
$value     = $readings->getLatestValueForSensorInDateRange  ($sensorid,  $to, $from)
$readings  = $readings->getLatestForSensorsInDateRange      ($sensorids, $to, $from)
$values    = $readings->getLatestValuesForSensorsInDateRange($sensorids, $to, $from)



// Return an array of unresponsive sensors by id.

/**
 * @type array|int[] $sensorids
 */
$sensorids = $readings->getUnresponsiveSensors();


// (or as identified at a specific moment in time).
$now = new DateTime();

$sensorids = $readings->getUnresponsiveSensors($now);
```

## Assumptions and unspecified behaviour

### Handling `PDOException`s is the responsibility of the calling application
* The `Readings` class does not attempt to catch any `PDOException`s.
* It is the responsiblity of the calling application to catch and manage these exceptions.
* For example, if the connection settings are invalid, a `PDOException` will be thrown and allowed to bubble up the stack trace.

### There will be no more than one reading taken for each sensor at any moment in time
* This is enforced by the unique key in the example database table.
* The `Readings` class _does not_ enforce this behaviour.
* In the event of an attempt to store a duplicate entry, a `PDOException` will be thrown.

### Handling 'to' and 'from' in date ranges
* 'to' is assumed to be exclusive from a date range; eg 2014-11-04 _is not_ in the range 2014-11-01 – 2014-11-04.
* 'from' is assumed to be inclusive of a date range; eg 2014-11-01 _is_ in the range 2014-11-01 – 2014-11-04.

## Repository content
* `config/*`: configuration files for unit and integration testing
* `resources/reading.sql`: SQL script to create the `reading` database table
* `src/*`: PHP files containing classes within the `ekkosense` namespace.
* `tests/unit/*`: unit testing classes (see below for execution instructions)
* `tests/integration/*`: integration testing classes (see below for execution instructions)

## Setting up a development and testing environment

The repository contains a `composer.json` file to aid the installation of the necessary development tools.

To set up a development environment, follow these steps:

```
#!sh
$ git clone https://bitbucket.org/throup/readings.git
$ cd readings
$ curl -sS https://getcomposer.org/installer | php
$ php composer.phar install
```

## Running the tests

There is a PHPUnit configuration file at `config/phpunit.xml`. To run the complete test suite, execute:

```
#!sh
$ vendor/bin/phpunit -c config/phpunit.xml
```

### Unit tests

The unit test suite exercises the methods of the Readings class, without making a real database connection. To run only unit
tests, execute:

```
#!sh
$ vendor/bin/phpunit -c config/phpunit.xml --testsuite unit
```

### Integration tests

The integration test suite uses a MySQL database connection to fully exercise the Readings class,
and to confirm the correct behaviours.

In order to run these tests, the following must be in place:

1. A MySQL schema must be available, with a table as described in `resources/reading.sql`.

2. Database connection settings must be specified in a config file, `config/integration.php`.
A sample config is included in `config/integration.php.dist`.

To run only integration tests, execute:

```
#!sh
$ vendor/bin/phpunit -c config/phpunit.xml --testsuite integration
```

### Using Phing

There is a Phing build script at `build.xml` which defines targets for all of the test suites.

```
#!sh
$ phing                    # executes both unit and integration test suites

$ phing unit-tests         # executes only the unit test suite

$ phing integration-tests  # executes only the integration test suite
```