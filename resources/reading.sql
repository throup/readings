CREATE TABLE reading (
  sensorid int(11) NOT NULL,
  datetime datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  reading double NOT NULL,
  PRIMARY KEY (sensorid,datetime),
  KEY sensorid (sensorid),
  KEY datetime (datetime)
);
